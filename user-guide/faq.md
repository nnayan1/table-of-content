[[_TOC_]]

##  Creating Input Files

##### Is there an upper limit to how many variables I can use in the Postman collection or the environment files?
No. There is no limit to how many variables can be used in the collection. DTP will solve all variables if they are defined in either the collection itself or the environment files.
##### Does the assessment of an endpoint depend on how the request is constructed?
No. Assessments that are run in DTP are not dependant on how the endpoints are called in the collection.

## Submitting a Job in DTP User Interface
##### How long should it take for a report to be generated?
It should not take more than 10 minutes for a report to be generated. If you do not receive it within 10 minutes, please report the bug. You can find more information on bug reporting from the FAQ's section 'Contact' and the left-hand menu item 'Give us Feedback!'.

## Understanding the Report
There are no frequently asked questions related to this topic at the moment.

## General
There are no frequently asked questions related to this topic at the moment.

## Contact
<!-- TO THE EDITOR OF THIS SECTION! If you edit anything from here, then be sure to update the document give-us-feedback.md! The information is duplicated to those two places, so if a user wants to contact the DTP team, then they are sure to find the information -->

##### I cannot find an answer to my question from the FAQ. Where can I ask for help?
All questions related to using DTP are welcome in the Slack channel dtp-alpha-support (contact [Cassie Wheeler](mailto:Cassandra.Wheeler@T-Mobile.com) to get access to the channel). We respond to questions on workdays from 8 AM to 4 PM PST in up to four hours.

#####  I found a bug when using DTP. How can I report it so that it would be fixed?
If you found a bug, please report it in the Slack channel dtp-alpha-report-defect (contact [Cassie Wheeler](mailto:Cassandra.Wheeler@T-Mobile.com) to get access to the channel). We really appreciate users reporting bugs. All bugs are registered and prioritized, and we respond to the reports in up to four hours on workdays from 8AM to 4PM PST.

**NB!** As the DTP team needs to first recreate the bug in order to fix it, it is essential that you provide us with the details regarding the bug. The information we need is the following:


- Please describe the error in your own words. What was wrong or what wasn't working like it was supposed to?
- On which date and at what time did the error occur?
- Please forward the Swagger link to us and send the Postman collection and environment you provided in the “Request form” step of the DTP User Interface.
- Please tell us if you changed the Payload in the “Payload review” step of the DTP User Interface. If you did, please describe your changes and copy the resulting Payload into a separate text file and send us that file.
- Please inform us if you managed to receive the assessment report.
  - If you did, please send us the report
  - If you did not, please send us the information (status, the ID of the job, and the time the job was submitted) provided to you in the “Output message” step of the DTP User Interface.

##### I have an idea for making the DTP more useful. How can I submit a feature request?
If you have an ideas for improving the DTP, please tell us about it in the Slack channel dtp-alpha-request-feature (contact [Cassie Wheeler](mailto:Cassandra.Wheeler@T-Mobile.com) to get access to the channel). DTP is still in development and the DTP team does its best to make the application as useful as possible. All feature requests are registered and prioritized.

For the DTP team to fully understand your feature request:

- Please describe the need for a feature request in detail. All kinds of pictures, diagrams and any other kind of information illustrating the new feature's functionality are welcome.
- **NB!** Describe the necessity of the feature request. It is essential that the DTP team understands the background behind the feature request to prioritize it correctly.